#!/bin/bash
# script to start all compose files for fichte

echo "starting all docker/podman compose files for fichte"

echo "beginning with zeebe"
cd zeebe || exit
podman-compose up zeebe-compose.yml
cd ..
cd kafka || exit
podman-compose up kafka-compose.yml


