#!/bin/bash

cd zeebe || exit
podman-compose down
cd ..
cd kafka || exit
podman-compose down
