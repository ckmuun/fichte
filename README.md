 Welcome to FICHTE! 

General note:
This is a work in progress which I work on during my freetime. Please do not consider this finished and/or production-grade software.

Furthermore, this project is *very* polyglot. It incorporates several languages and frameworks (Java, Scala, Typescript, Spring Boot, Quarkus, Angular and Apache Spark). I do not generally advise for using such a wide stack in a production scenario.

FICHTE stands for Flower Image Classification Human and Technology Experiment ('Fichte' is the german word for a spruce tree)

This project is about classifying five types of flowers from the relatively well-known flowers dataset from kaggle.com.
However, this is not a data science project in the traditional sense, although ML classification with Apache Spark is part of it.
This project is about how classification of documents (images right now, later more types) by machines and classification by humans can be combined into one application.

The idea is that many ML applications do quite fine in handling common cases during document classification, but struggle with edge cases and junk documents. FICHTE aims to create an application that integrates ML and humans as seamlessly as possible.


Setup instructions:

Important note: As this is a work in progress, unexpected problems when cloning and developing locally may occur. 

I generally recommend to use a Linux distro for these steps.

I recommend using SDKMAN! (https://sdkman.io/) to install these SDKs: 
1. You need Apache Spark. For non-skdman installation instructions see https://spark.apache.org 
2. You need Java, different versions (8,11, 13), also Maven 3.6.x
3. You need Scala 2.11 SDK (not 2.12!) 
4. You need Docker/Podman for the containers in fichte-scripts 
5. Planned: Usage of K8S / Minikube locally.

Roadmap / Milestones:

1. Initial version of the 'Flower Classifier' allowing the interaction of humans and ml models to classify images.
2. Integration of mimetype-based document and feature extraction. 
-> Upload PDF containing flower image and classify it the same way as you would a png file.
3. Setup for more types of documents, apart from images. For example text, formats, receipts. Generally implement a broader "classification toolbox"   
4. Improve flexibility with a management API that allows to see labels, define new labels and trigger (re)training 
of models. 
5. Improve system to autonomously decide on labels and simultaneously adapt via retraining in order to build a truly 
self-learning and flexible system. 


