package de.koware;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/emit")
public class ExampleResource {

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String echo(String requestBody) {
        return requestBody;
    }
}
