package de.koware.emitter;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Path("/client")
public class FlowerEmitterImpl {

    @Inject
    @RestClient
    FlowerEmitterService service;


    @POST
    @Path("/multipart")
    @Produces(MediaType.TEXT_PLAIN)
    public String emitFlower() throws FileNotFoundException {
        FlowerMpBody flowerMpBody = new FlowerMpBody();
        flowerMpBody.fileName = "dorban-lr";
        flowerMpBody.file = new FileInputStream(new File("flowers-to-emit/dandelion/7355522_b66e5d3078_m.jpg"));

        return this.service.periodicallyEmitFlower(flowerMpBody);
    }
}
