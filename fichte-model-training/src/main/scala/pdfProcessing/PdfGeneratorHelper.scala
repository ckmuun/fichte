package pdfProcessing

import classificationSc.FlowersCfConfig
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.slf4j.{Logger, LoggerFactory}

object PdfGeneratorHelper {

  private val LOGGER: Logger = LoggerFactory.getLogger(this.getClass)


  val sparkSession: SparkSession = FlowersCfConfig.sparkSession

  def createRddFromJobBulletins(): RDD[(String, String)] = {
    LOGGER.info("creating dataset")

    sparkSession.sparkContext.wholeTextFiles("/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinsDs/cityofla/CityofLA/Job Bulletins")
  }

  def createPdfFileFromJobBulletin(textContent: String): PDDocument = {

    LOGGER.info("creating pdf documents from Job bulletins")

    val rdd = this.createRddFromJobBulletins()





    new PDDocument()
  }
}
