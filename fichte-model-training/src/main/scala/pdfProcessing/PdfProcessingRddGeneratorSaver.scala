package pdfProcessing

import java.awt.image.{BufferedImage, RenderedImage}
import java.io.{ByteArrayOutputStream, File}
import java.nio.file.{Files, Paths}
import java.util
import collection.JavaConverters._
import javax.imageio.ImageIO
import classificationSc.FlowersCfConfig
import org.apache.pdfbox.pdmodel.font.{PDFont, PDType1Font}
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import org.apache.pdfbox.pdmodel.{PDDocument, PDPage, PDPageContentStream}
import org.apache.pdfbox.rendering.PDFRenderer
import org.apache.spark.sql.{Column, DataFrame, Dataset, Encoders, Row}
import rst.pdfbox.layout.elements.{Document, Paragraph}

object PdfProcessingRddGeneratorSaver {

  private val sparkSession = FlowersCfConfig.sparkSession


  @Deprecated
  def createBulletinsListWithPdfSave(): util.ArrayList[String] = {

    val bulletinsList = new util.ArrayList[String](150)

    val bulletinsDirFiles: Array[File] = loadBulletinsDirFiles

    bulletinsDirFiles.foreach(file => {
      println("reading file: " + file.getAbsolutePath)

      val lines: util.List[String] = Files.readAllLines(file.toPath)

      var bulletin = ""
      val pdf = new PDDocument()
      val page: PDPage = new PDPage()

      val contentStream: PDPageContentStream = new PDPageContentStream(pdf, page)
      contentStream.beginText()
      contentStream.setFont(PDType1Font.TIMES_ROMAN, 8)

      contentStream.newLineAtOffset(25, 800)
      contentStream.setLeading(14.5f)

      val pageWidth = page.getMediaBox.getWidth

      page.getMediaBox.getHeight


      // put formatted lines into pdpage
      val iter = lines.iterator()
      while (iter.hasNext) {
        // vars setup
        var line = iter.next()

        line = line.replace("\t", "")

        try {
          contentStream.showText(line)
          contentStream.newLine()
        } catch {
          // some text bulletins contain random unicode chars PDFBOX can't handle without fiddling around.
          // as we have around 900 bulletins, we just skip these files and go on.
          case iae: IllegalArgumentException => iae.printStackTrace()
        }

        bulletin = bulletin.concat(line)
      }

      contentStream.endText()
      contentStream.close()
      pdf.addPage(page)
      bulletinsList.add(bulletin)
      pdf.save(new File("/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinPdfs/" + System.currentTimeMillis() + ".pdf"))
      pdf.close()

    })


    bulletinsList
  }

  private def loadBulletinsDirFiles: Array[File] = {


    val bulletinsDir = new File("/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinsDs/cityofla/CityofLA/Job Bulletins")

    assert(bulletinsDir.isDirectory)
    val bulletinsDirFiles: Array[File] = bulletinsDir.listFiles()

    bulletinsDirFiles
  }

  def createBulletinsList(): util.ArrayList[String] = {
    val bulletinsList = new util.ArrayList[String](900)

    val bulletinsDirFiles = loadBulletinsDirFiles

    bulletinsDirFiles.foreach(file => {
      println("reading file: " + file.getAbsolutePath)

      val lines: util.List[String] = Files.readAllLines(file.toPath)

      var bulletin = ""

      // put formatted lines into pdpage
      val iter = lines.iterator()
      while (iter.hasNext) {
        // vars setup
        var line = iter.next()

        line = line.replace("\t", "")

        bulletin = bulletin.concat(line)
      }
      bulletinsList.add(bulletin)
    })


    bulletinsList
  }

  private def createLayoutedPdfFromBulletin(bulletinText: String): Unit = {

    println("creating layouted pdf")
    val pdfWithLayout: Document = new Document()

    val paragraph: Paragraph = new Paragraph()
    paragraph.addText(bulletinText, 11, PDType1Font.TIMES_ROMAN)
    pdfWithLayout.add(paragraph)

    pdfWithLayout.save(new File("/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinPdfs/" + System.currentTimeMillis() + "-layout.pdf"))
  }


  def main(args: Array[String]): Unit = {

    //#val rdd: RDD[(String, String)] = PdfGeneratorHelper.createRddFromJobBulletins()
    //rdd.saveAsObjectFile("jobBulletinsDs/dataset/" + System.currentTimeMillis())
    //rdd.saveAsTextFile("/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinsDs/dataset/" + System.currentTimeMillis())

    val bulletinList: Seq[String] = createBulletinsList().asScala

    bulletinList.foreach(bulletin => createLayoutedPdfFromBulletin(bulletin))

    implicit val stringEncoder = Encoders.STRING

    val dataset: Dataset[String] = sparkSession.createDataset(bulletinList)


    dataset.show(false)

  }

  def createEmbeddedImagePdfFromTextPdf(pdf: PDDocument): PDDocument = {
    println("creating embedded image pdf from embedded text pdf")

    val imagePdf = new PDDocument()

    val renderer: PDFRenderer = new PDFRenderer(pdf)
    val image = renderer.renderImage(0)

    val baos = new ByteArrayOutputStream()
    ImageIO.write(image, "jpg", baos)

    val bytes = baos.toByteArray

    val page: PDPage = new PDPage()
    val pageContentStream = new PDPageContentStream(imagePdf, page)
    val imagePdfObject: PDImageXObject = PDImageXObject.createFromByteArray(imagePdf, bytes, "pdf-as-image")
    pageContentStream.drawImage(imagePdfObject, 50, 50)
    pageContentStream.close()
    imagePdf
  }

  //  def loadBulletinsSparkStyle(): Dataset[Row] = {
  //    val textFileDir = "/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinsDs/cityofla/CityofLA/Job Bulletins/*"
  //    //val textFileDir = "/home/cornelius/SoftwareProjects/fichte/fichte-model-training/src/main/resources/jobBulletinsDs/dataset/1604857617356"
  //
  //    val flowersDfAll = sparkSession.read.textFile(textFileDir)
  //
  //    println("showing dataset")
  //    //    LOGGER.info("showing dataset")
  //    flowersDfAll.show(false)
  //
  //    println("number of ds rows: " + flowersDfAll.count())
  //
  //    val rows = flowersDfAll.collect()
  //
  //
  //    val fileDs = sparkSession.emptyDataset(Encoders.STRING)
  //
  //    null
  //  }
  //

}
