package classificationSc

import classificationSc.FlowersModelGenerator.loadLabelFeaturizeDs
import org.apache.spark.sql.{DataFrame, SparkSession}

object FlowerRandomForestGenerator {


  def main(args: Array[String]): Unit = {
    val sparky: SparkSession = FlowersCfConfig.sparkSession
    val flowersDir = "flowers-model-training/src/main/resources/flowers/*"

    // use image data source or binary file data source for images https://docs.databricks.com/data/data-sources/image.html
    val flowersDfAll: DataFrame = sparky.read.format("image").load(flowersDir)


    flowersDfAll.write.mode("overwrite").json("flowers-model-training/src/main/resources/randomForest/flowersAsDs")

    val pipelineModel = loadLabelFeaturizeDs(flowersDfAll)

    // val tfDs = pipelineModel.transform(sparky.read.json("flowers/src/main/resources/flowersAsDs/part-00135-d200b7b8-6864-4c34-9c9b-353611413021-c000.json").as(Encoders.bean(ImageSchema.getClass)))
    //tfDs.printSchema()
    //tfDs.show(truncate = true)
    println("done")
  }
}
