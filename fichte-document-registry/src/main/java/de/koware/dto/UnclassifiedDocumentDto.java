package de.koware.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.springframework.util.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public class UnclassifiedDocumentDto implements Serializable {

    private byte[] data;

    @JsonIgnore
    private InputStream documentInputStream;

    private String mime;
    private String filename;


    @JsonCreator
    private UnclassifiedDocumentDto() {

    }


    public UnclassifiedDocumentDto(InputStream documentInputStream, String mime, String filename) throws IOException {
        this.documentInputStream = documentInputStream;
        this.mime = mime;
        this.filename = filename;
        this.setData(StreamUtils.copyToByteArray(documentInputStream));

    }


    public InputStream getDocumentInputStream() {
        return documentInputStream;
    }

    @JsonGetter
    public String getMime() {
        return mime;
    }

    @JsonGetter
    public String getFilename() {
        return filename;
    }

    public byte[] getData() {
        return data;
    }

    @JsonSetter
    private void setData(byte[] data) {
        this.data = data;
        this.documentInputStream = new ByteArrayInputStream(data);
    }


    @JsonSetter
    private void setMime(String mime) {
        this.mime = mime;
    }

    @JsonSetter
    private void setFilename(String filename) {
        this.filename = filename;
    }
}
