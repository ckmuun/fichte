package de.koware.dto;

import java.io.IOException;

public class UnclassifiedDocument {

    private byte[] data;
    private String mime;
    private String filename;


    public UnclassifiedDocument(UnclassifiedDocumentDto documentDto) throws IOException {
        this.setData(documentDto.getData());
        this.setFilename(documentDto.getFilename());
        this.setMime(documentDto.getMime());
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
