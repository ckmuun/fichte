package de.koware.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSetter;

public class NotClassifiableDocumentDto extends ProcessedDocumentDto {
    private DocumentAttachmentDto rawDoc;


    public NotClassifiableDocumentDto(DocumentAttachmentDto documentAttachmentDto) {
        this.rawDoc = documentAttachmentDto;
    }

    @JsonCreator
    private NotClassifiableDocumentDto () {

    }

    public DocumentAttachmentDto getRawDoc() {
        return rawDoc;
    }

    @JsonSetter
    private void setRawDoc(DocumentAttachmentDto rawDoc) {
        this.rawDoc = rawDoc;
    }

}
