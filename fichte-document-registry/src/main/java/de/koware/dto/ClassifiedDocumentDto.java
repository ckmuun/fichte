package de.koware.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class ClassifiedDocumentDto extends ProcessedDocumentDto {

    private DocumentAttachmentDto documentAttachmentDto;
    private String label;


    public ClassifiedDocumentDto(String label, DocumentAttachmentDto attachmentDto) {
        this.documentAttachmentDto = attachmentDto;
    }

    @JsonCreator
    private ClassifiedDocumentDto() {

    }


    @JsonSetter
    private void setDocumentAttachmentDto(DocumentAttachmentDto documentAttachmentDto) {
        this.documentAttachmentDto = documentAttachmentDto;
    }


    @JsonGetter
    private DocumentAttachmentDto getDocumentAttachmentDto() {
        return this.documentAttachmentDto;
    }


}
