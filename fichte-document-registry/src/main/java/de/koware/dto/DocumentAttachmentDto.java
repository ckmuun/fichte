package de.koware.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.apache.tika.Tika;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class DocumentAttachmentDto {


    private long size;
    private byte[] bytes;
    private String originalFilename;
    private String name;
    private String contentType;
    private UUID correlationKey;
    private UUID id;

    @JsonIgnore
    private InputStream inputStream;


    @JsonCreator
    private DocumentAttachmentDto() {
    }

    public DocumentAttachmentDto(InputStream inputStream, String filename) throws IOException {
        this.setName(filename);
        this.setOriginalFilename(filename);
        this.inputStream = inputStream;
        Tika tika = new Tika();
        this.contentType = tika.detect(inputStream, filename);
     //   this.setBytes(StreamUtils.scopyToByteArray(inputStream));
        this.setSize(this.getBytes().length);
    }

    public long getSize() {
        if (null == this.bytes) {
            return 0;
        }
        return bytes.length;
    }


    @JsonSetter
    private void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @JsonSetter
    private void setSize(long size) {
        this.size = size;
    }

    @JsonSetter
    private void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @JsonSetter
    private void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    @JsonSetter
    private void setName(String name) {
        this.name = name;
    }

    @JsonGetter
    public byte[] getBytes() throws IOException {
        return bytes;
    }

    @JsonGetter
    public String getOriginalFilename() {
        return originalFilename;
    }

    @JsonGetter
    public String getContentType() {
        return contentType;
    }

    @JsonGetter
    public String getName() {
        return name;
    }

    @JsonIgnore
    public InputStream getInputStream() {
        return inputStream;
    }
}
